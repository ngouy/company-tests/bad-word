let list = {
  'e': ['3'],
  'a': ['@', 'V'],
}

let replace_letters = (word) => {
  if (!word.length) {
    return [];
  }
  if (word.length == 1) {
    let result = (list[word] || []).slice(0);
    result.push(word);
    return result;
  }
  let result = [word];
  let rez = replace_letters(word.slice(1))
  rez.forEach(wordd => {
    let _list = (list[word[0]] || []).slice(0);
    _list.push(word[0]);
    _list.forEach(letter => {
      result.push(letter + wordd);
    });
  });
  return [...new Set(result)];
}

bad_world_list = replace_letters('carker');